package com.example.demo.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldController {


    @GetMapping("/helloWorld")
    public String helloWorld(){
        return "helloWorld";
    }

    @GetMapping("/helloWorld/{name}")
    public String helloWorldName(@PathVariable(value="name")String name ){
        return "hello " + name;
    }

    @PostMapping("/helloWorld")
    public String helloWorldPostController(@RequestParam (name = "username")String username ){
        return "hello " + username;

    }


}
